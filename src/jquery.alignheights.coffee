do ($ = jQuery) ->

  ###*
   * 高さ揃え
   *
   * @param [config] {Object} オプション
   * @param [config.groupNum = 全件] {Number} 高さを揃えるグループの個数
   * @param [config.interval = 250] {Number} 処理を繰り返す間隔（ミリ秒）
   * @param [config.endless = false] {Boolean} ずっと繰り返す
   * @param [config.ignoreImageLoadError = true] {Boolean} 画像の読み込みエラーを無視する（画像の読み込みエラーでも処理を完了する）
   * @param [config.isSkipSameHeight = true] {Boolean} 要素の高さが同じ場合、高さを設定しない
   * @return jQueryセレクタ
  ###
  $.fn.alignHeights = (config = {}) ->

    #----------------------------------
    #  variables
    #----------------------------------
    _me = this
    _numItem = this.length #要素数
    _elements = []  #要素のストック
    _maxHeight = 0  #要素内の最大の高さ
    _intervalTimer = NaN  #繰り返し用のタイマー

    #オプション値のマージ
    o = $.extend({
      groupNum: _numItem
      interval: 250
      endless: false
      ignoreImageLoadError: true
      isSkipSameHeight: true
    }, config)


    ###*
     * 要素の高さをまとめて揃える
     *
     * @param elements {Array} 要素
     * @param height {Number} 高さ
     * @param ignoreImageLoadError {Boolean} 画像の読み込みエラーを処理完了と判断する
     * @param isSkipSameHeight {Boolean} 要素の高さが同じなら高さを設定しない
     * @return {Boolean} 処理が完了したか
    ###
    setTallestHeight = (elements, height, ignoreImageLoadError, isSkipSameHeight) ->
      len = elements.length #要素数
      isSameHeight = true #高さが同じか
      result = true  #処理結果

      if len > 0
        for element, index in elements
          images = $("img", element) #要素内のimgタグを取得
          for img, index2 in images
            if img.complete != true || (ignoreImageLoadError == false && img.height == 0)
              #画像の読み込みが完了していない。（画像読み込みエラーを無視しない場合は、それも考慮する）
              #→処理を中止
              result = false
              break

          #要素の高さが同じかを判別
          if isSkipSameHeight == true
            elementHeight = $(element).height()

            if height != elementHeight
              isSameHeight = false  #指定の高さと違うものがあれば、フラグを折る

      if images && result == true && (isSkipSameHeight == false || isSameHeight == false)
        #高さを揃える
        for element, index in elements
          $(element).height(height)

      return result


    ###*
     * 要素の高さをまとめて揃える
     *
    ###
    alignHeight = ->
      isFinish = true #終了したか
      result = false  #処理結果

      if _elements.length == 0
        isFinish = true

      $(_me).each((index) ->
        $this = $(this)
        _elements.push(this)  #要素をストック
        
        #一番高い高さを検出する
        height = Math.round($this.height()) #Firefox（それ以外のブラウザでも？）で小数点になることがあるので丸めておく
        if height> _maxHeight
          _maxHeight = height
          
        if _elements.length >= config.groupNum || index >= _numItem - 1
          #規定要素数 or 要素が全部になったら
          if _maxHeight > 0
            result = setTallestHeight(_elements, _maxHeight, o.ignoreImageLoadError, o.isSkipSameHeight)

          #値をリセット
          _maxHeight = 0
          _elements = []
          
          # すべての高さ揃えが完了していなければ、フラグを折っておく
          if result == false
            isFinish = false
        return
      )
      
      # 全ての要素の高さ揃えが完了した？
      if isFinish == true && config.endless == false
        clearInterval(_intervalTimer)
        _intervalTimer = NaN
      return


    ###*
     * 処理を停止する
     *
    ###
    stop = ->
      if isNaN(_intervalTimer) == false
        clearInterval(_intervalTimer)
      _intervalTimer = NaN
      return


    ###*
     * 初期化
     *
    ###
    init = ->
      if isNaN(_intervalTimer) == false
        clearInterval(_intervalTimer)

      _intervalTimer = setInterval(alignHeight, config.interval)
      alignHeight()
      return


    init()

    return this
  return