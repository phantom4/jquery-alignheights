jquery-alignheights.js 1.0.0
============================
要素の高さを揃えるjQueryプラグインです。




仕様
----
* タイマーで高さ揃えを処理
* 対象要素内に画像が存在する場合、画像の読み込みを待って処理

 
使い方
------
###Step1: ファイルの読み込み

```html
<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="js/jquery.alignheights.min.js"></script>
```

###Step2: マークアップ
```html
<ul class="hoge">
<li>1<br>2<br>3</li>
<li>A<br>B<br>C<br>D<br>E</li>
<li>a<br>b</li>
</ul>
```

###Step2: 呼び出し
```html
$(function () {
    $(".hoge li").alignHeights();
});
```
 
パラメーター
-----------

### groupNum
要素を揃える個数

```
default: 要素全て
options: integer
```

```html
//例: 3つずつ揃える
$(".hoge li").alignHeights({
    groupNum: 3
});
```


### interval
高さ合わせを実行する間隔（ミリ秒）

```
default: 250
options: integer
```


### endless
高さ揃えが完了した後も処理を続けるか

```
default: false
options: boolean (true or false)
```


### ignoreImageLoadError
対象要素内にある画像がリンク切れの場合、それを無視して高さ揃えを実行するか

```
default: true
options: boolean (true or false)
```


### isSkipSameHeight
グループ内の要素の高さが同じ場合（すべて揃っている場合）、高さ指定をしない

```
default: true
options: boolean (true or false)
```



今後の予定
----------
* 実行中の処理を止めるメソッドを用意する。
* TypeScript版を作成する。

 
ライセンス
----------
Copyright &copy; 2015