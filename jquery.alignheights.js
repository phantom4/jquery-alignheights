(function($) {

  /**
   * 高さ揃え
   *
   * @param [config] {Object} オプション
   * @param [config.groupNum = 全件] {Number} 高さを揃えるグループの個数
   * @param [config.interval = 250] {Number} 処理を繰り返す間隔（ミリ秒）
   * @param [config.endless = false] {Boolean} ずっと繰り返す
   * @param [config.ignoreImageLoadError = true] {Boolean} 画像の読み込みエラーを無視する（画像の読み込みエラーでも処理を完了する）
   * @param [config.isSkipSameHeight = true] {Boolean} 要素の高さが同じ場合、高さを設定しない
   * @return jQueryセレクタ
   */
  $.fn.alignHeights = function(config) {
    var _elements, _intervalTimer, _maxHeight, _me, _numItem, alignHeight, init, o, setTallestHeight, stop;
    if (config == null) {
      config = {};
    }
    _me = this;
    _numItem = this.length;
    _elements = [];
    _maxHeight = 0;
    _intervalTimer = NaN;
    o = $.extend({
      groupNum: _numItem,
      interval: 250,
      endless: false,
      ignoreImageLoadError: true,
      isSkipSameHeight: true
    }, config);

    /**
     * 要素の高さをまとめて揃える
     *
     * @param elements {Array} 要素
     * @param height {Number} 高さ
     * @param ignoreImageLoadError {Boolean} 画像の読み込みエラーを処理完了と判断する
     * @param isSkipSameHeight {Boolean} 要素の高さが同じなら高さを設定しない
     * @return {Boolean} 処理が完了したか
     */
    setTallestHeight = function(elements, height, ignoreImageLoadError, isSkipSameHeight) {
      var element, elementHeight, i, images, img, index, index2, isSameHeight, j, k, len, len1, len2, len3, result;
      len = elements.length;
      isSameHeight = true;
      result = true;
      if (len > 0) {
        for (index = i = 0, len1 = elements.length; i < len1; index = ++i) {
          element = elements[index];
          images = $("img", element);
          for (index2 = j = 0, len2 = images.length; j < len2; index2 = ++j) {
            img = images[index2];
            if (img.complete !== true || (ignoreImageLoadError === false && img.height === 0)) {
              result = false;
              break;
            }
          }
          if (isSkipSameHeight === true) {
            elementHeight = $(element).height();
            if (height !== elementHeight) {
              isSameHeight = false;
            }
          }
        }
      }
      if (images && result === true && (isSkipSameHeight === false || isSameHeight === false)) {
        for (index = k = 0, len3 = elements.length; k < len3; index = ++k) {
          element = elements[index];
          $(element).height(height);
        }
      }
      return result;
    };

    /**
     * 要素の高さをまとめて揃える
     *
     */
    alignHeight = function() {
      var isFinish, result;
      isFinish = true;
      result = false;
      if (_elements.length === 0) {
        isFinish = true;
      }
      $(_me).each(function(index) {
        var $this, height;
        $this = $(this);
        _elements.push(this);
        height = Math.round($this.height());
        if (height > _maxHeight) {
          _maxHeight = height;
        }
        if (_elements.length >= config.groupNum || index >= _numItem - 1) {
          if (_maxHeight > 0) {
            result = setTallestHeight(_elements, _maxHeight, o.ignoreImageLoadError, o.isSkipSameHeight);
          }
          _maxHeight = 0;
          _elements = [];
          if (result === false) {
            isFinish = false;
          }
        }
      });
      if (isFinish === true && config.endless === false) {
        clearInterval(_intervalTimer);
        _intervalTimer = NaN;
      }
    };

    /**
     * 処理を停止する
     *
     */
    stop = function() {
      if (isNaN(_intervalTimer) === false) {
        clearInterval(_intervalTimer);
      }
      _intervalTimer = NaN;
    };

    /**
     * 初期化
     *
     */
    init = function() {
      if (isNaN(_intervalTimer) === false) {
        clearInterval(_intervalTimer);
      }
      _intervalTimer = setInterval(alignHeight, config.interval);
      alignHeight();
    };
    init();
    return this;
  };
})(jQuery);
