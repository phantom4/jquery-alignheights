'use strict'

# Load all required libraries.
gulp = require "gulp"
plugins = require("gulp-load-plugins")()
bower = require "bower"
config = require("./package.json").config #configファイルを読み込み
path = require("path")



#----------------------------------
#  modules
#----------------------------------


###*
 * jsまたはcoffeeを結合して、未圧縮版と圧縮版を生成
 *
 * @param src {Array|String} 対象のjs（coffee, tsを含む）ファイル
 * @param joinName {String} 結合後のファイル名
 * @param dest {String} 生成先のディレクトリ
 * @param coffeeOption {Object} coffee scriptのコンパイル設定
 * @param typescriptOption {Object} typescriptのコンパイル設定
 * @return {Object} gulp
###
compileJS = (src, joinName, dest, coffeeOption = {}, typescriptOption = {}) ->
  filterCoffee = plugins.filter((file) ->
    return /\.coffee$/.test(file.path)
  , {
    restore: true
  }) #coffeeファイルのフィルタ
  filterTypescript = plugins.filter("**/*.ts", {
    restore: true
  }) #typescriptファイルのフィルタ

  gulp
  .src src
  .pipe plugins.plumber({
    errorHandler: plugins.notify.onError('<%= error.message %> <%= error %>')
  })

  #coffeeのコンパイル
  .pipe filterCoffee
  .pipe plugins.coffee(coffeeOption)
  .pipe filterCoffee.restore

  #typescriptのコンパイル
  .pipe filterTypescript
  .pipe plugins.typescript(typescriptOption)
  .pipe filterTypescript.restore

  .pipe plugins.concat joinName
  .pipe gulp.dest dest  #未圧縮を生成
  .pipe plugins.notify({
    message: "Generated file: <%= file.relative %> \t <%= file.path %>"
  })
  .pipe plugins.uglify().on("error", plugins.util.log)
  .pipe plugins.rename({suffix: ".min"})
  .pipe gulp.dest dest  #圧縮版を生成
  .pipe plugins.notify({
    message: "Generated file: <%= file.relative %> \t <%= file.path %>"
  })



###*
 * エラー表示
 *
 * @param error {Object}
###
errorHandler = (error) ->
  console.log error
  return


###*
 * ページ別のjs、css、画像最適化のタスクを生成する
 *
 * @param taskName {String} タスク名
 * @param process {Object} 処理
 * @return {Object} 設定されたタスク
###
execTask = (processName, process) ->
  switch processName

    when "js"
      #jsファイル（js, coffee-script, typescript）のコンパイル＆結合
      for dest, files of process
        for fileName, file of files
          src = file.src
          if src? && src.length > 0
            compileJS(src, fileName, dest, file["coffee_option"], file["typescript_option"])

    else
      #何もしない

  return this


###*
 * 判定チェック
 *
 * @param type {String} チェックする型（String, Number, Boolean, Date, Error, Array, Function, RegExp, Object）
 * @param obj {*} チェックするデータ
 * @return
###
isType = (type, obj) ->
  clas = Object.prototype.toString.call(obj).slice(8, -1)
  return obj != undefined && obj != null && clas == type


###*
 * jsonのマージ
 *
 * @param obj1 {Object}
 * @param obj2 {Object}
 * @return {Object} マージ後のデータ
###
extend = (obj1, obj2 = {}) ->
  data = obj1
  for key, value of obj2
    if obj2.hasOwnProperty(key)
      data[key] = value
  return data


###*
 * 初期化
 *
###
init = ->
  #タスクをpackage.jsonから生成
  func = {}

  #ページ別のタスク
  for page, settings of config.pages

    #ページ別のタスク
    gulp.task("#{page}", () ->
      me = this.seq[0].split(":")  #タスク名を取得
      for taskName, task of config.pages[me[0]]
        if taskName != "watch"
          execTask(taskName, task)
    )

    for taskName, task of settings
      if taskName != "watch"

        #ページ＆機能別のタスク
        gulp.task("#{taskName}:#{page}", () ->
          me = this.seq[0].split(":")  #タスク名を取得
          pageName = me[1]
          taskName = me[0]
          execTask(taskName, config.pages[pageName][taskName])
        )

        #機能別のタスクのためにまとめておく
        if func[taskName]? == false
          func[taskName] = []
        func[taskName].push "#{taskName}:#{page}"

  #機能別のタスク
  for taskName, task of func
    gulp.task("#{taskName}", () ->
      me = this.seq[0]  #タスク名を取得
      for page, settings of config.pages
        for taskName, task of settings
          if taskName == me
            execTask(taskName, config.pages[page][taskName])
    )

  return


do ->
  init()
  return



#----------------------------------
#  task
#----------------------------------
gulp.task("default", () ->
  #gulpconfig.jsonから動的に生成する
  for page, settings of config.pages
    for taskName, task of settings
      if taskName != "watch"
        execTask(taskName, task)
)


gulp.task("watch", () ->
  #gulpconfig.jsonから動的に生成する
  for page, settings of config.pages
    for taskName, src of settings["watch"]
      gulp.watch src, ["#{taskName}:#{page}"]
)


gulp.task("help", plugins.taskListing)